﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class TilesController : MonoBehaviour {
	public GameObject tile;
	public float waitTime;
	public bool clickable;
	public int numOfTries;
	public int correctClicked;
	public bool isThisEnd;
	public bool showSecondTime;
	public bool gravity;
	public GameObject pointsPlus;


    public float rotationTime = 0.5f;

    private int finalRotation;
    private bool rotating;

	private bool pointsAdded;
	private int numOfTilesWidth;
	private int numOfTilesHeight;
	private int numOfCorrectTiles;
	private float startTime;
	private float actualWidth;
	private float actualHeight;
	private float startWidth;
	private float startHeight;
	private int actualI;
	private string side;
	private float timeToDestroy;
    
	void Start ()
    {
        SetupVariables();

        int numOfTiles = numOfTilesWidth * numOfTilesHeight;
        SetStartingPosition();

        int[] correct = SetRandomCorrectTiles(numOfTiles);
        CreateTiles(numOfTiles, correct);
        RescaleTiles();
        SetFinalRotation();
        SetCorrectTilesCounterOnUI();
    }
    
    void Update()
    {
        //ROTATE
        RotateTiles();
        CheckForTheEnd();
    }


    private void SetCorrectTilesCounterOnUI()
    {
        GameObject h = GameObject.FindGameObjectWithTag("TilesUi");
        TilesUiController otherScripth = h.GetComponent<TilesUiController>();
        otherScripth.tiles = numOfCorrectTiles;
    }

    private void SetStartingPosition()
    {
        SpriteRenderer r = tile.GetComponent<SpriteRenderer>();
        //POSITION OF ALL TILES
        actualWidth = 0.0f - (numOfTilesWidth * 0.5f * r.bounds.size.x) + (r.bounds.size.x * 0.5f);
        startWidth = actualWidth;
        actualHeight = 0.0f + (numOfTilesHeight * 0.5f * r.bounds.size.y) - (r.bounds.size.y * 0.5f);
        startHeight = actualHeight;
    }

    private int[] SetRandomCorrectTiles(int numOfTiles)
    {
        ArrayList numbers = new ArrayList();
        for (int k = 1; k <= numOfTiles; k++)
        {
            numbers.Add(k);
        }
        int[] correct = new int[numOfCorrectTiles + 1];
        for (int j = 1; j <= numOfCorrectTiles; j++)
        {
            int thisNumber = Random.Range(0, numbers.Count);
            correct[j] = (int)numbers[thisNumber];
            numbers.RemoveAt(thisNumber);
        }

        return correct;
    }

    private void SetFinalRotation()
    {
        int temp = Random.Range(1, 10);
        if (temp > 5)
        {
            side = "right";
        }
        else
        {
            side = "left";
        }
        temp = Random.Range(1, 2);
        if (side == "right")
        {
            finalRotation = temp * -90;
        }
        else
        {
            finalRotation = temp * 90;
        }
    }

    private void RescaleTiles()
    {
        float tempSize = ((4.86f / numOfTilesWidth)) / 2.7f;
        this.gameObject.transform.localScale = new Vector3(tempSize, tempSize, 1.0f);
    }

    private void CreateTiles(int numOfTiles, int[] correct)
    {
        actualI = 0;
        for (int i = 1; i <= numOfTiles; i++)
        {
            actualI++;
            //Debug.Log(actualI);
            GameObject tileGameObject = Instantiate(tile);
            tileGameObject.transform.parent = this.transform;
            for (int l = 1; l <= numOfCorrectTiles; l++)
            {
                InstantiateTileController script = tileGameObject.GetComponent<InstantiateTileController>();
                if ((int)correct[l] == i)
                {
                    script.isThisRightTile = true;
                }
                script.tileNumber = i;
            }
            //POSITION FOR THIS TILE


            tileGameObject.transform.position = new Vector3(actualWidth, actualHeight, 0.0f);
            SpriteRenderer renderer = tileGameObject.GetComponent<SpriteRenderer>();
            // NEXT POS X
            //			Debug.Log("Size.x :"+renderer.bounds.size.x);
            actualWidth = renderer.bounds.size.x + renderer.transform.position.x;
            // NEXT POS Y
            if (actualI == numOfTilesWidth)
            {
                actualI = 0;
                actualWidth = startWidth;
                actualHeight = renderer.transform.position.y - renderer.bounds.size.y;
            }
        }
    }

    private void SetupVariables()
    {
        timeToDestroy = 0;
        pointsAdded = false;
        clickable = false;
        gravity = false;
        isThisEnd = false;
        showSecondTime = false;
        startTime = Time.time;
        numOfTries = 0;

        GameObject g = GameObject.FindGameObjectWithTag("Game");
        GameController otherScript = g.GetComponent<GameController>();
        numOfTilesWidth = otherScript.numOfTilesWidth;
        numOfTilesHeight = otherScript.numOfTilesHeight;
        numOfCorrectTiles = otherScript.numOfCorrectTiles;
        otherScript.actualRound += 1;
    }
    
    private void RotateTiles()
    {
        if (waitTime + startTime < Time.time && !rotating)
        {
            rotating = true;
            transform.DORotate(new Vector3(0.0f, 0.0f, finalRotation), rotationTime)
                .OnComplete(() => clickable = true);
        }
    }
    
    private void CheckForTheEnd()
    {
        if (numOfTries == numOfCorrectTiles)
        {
            if (timeToDestroy == 0)
            {
                SetTimeToDestroy();
            }
            if (timeToDestroy - 1 < Time.time)
            {
                gravity = true;
                TryAddingEndPoints();
            }
            clickable = false;
            showSecondTime = true;
            if (timeToDestroy < Time.time)
            {
                isThisEnd = true;
            }
        }
    }

    private void TryAddingEndPoints()
    {
        if (pointsAdded == false)
        {
            pointsAdded = true;
            if (correctClicked == numOfTries)
            {
                GameObject h = GameObject.FindGameObjectWithTag("Points");
                PointsController otherScripth = h.GetComponent<PointsController>();
                otherScripth.points += 20;
                Instantiate(pointsPlus);
            }
        }
    }

    private void SetTimeToDestroy()
    {
        timeToDestroy = Time.time + ((correctClicked == numOfTries) ? 1.5f : 3f);
    }
}
