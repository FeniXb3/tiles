﻿using UnityEngine;
using System.Collections;

public class HideController : MonoBehaviour {
	public GUIText thisText;
	// Use this for initialization
	void Start () {
		thisText.enabled = false;
	}
}
