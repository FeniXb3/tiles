﻿using UnityEngine;
using System.Collections;

public class TilesUiController : MonoBehaviour {
	public GUIText tilesText;
	public int tiles;
	//private int actualPoints;
	// Use this for initialization
	void Start () {
		tilesText.fontSize = Screen.height / 20;
		//pointsText.enabled = false;
		tiles = 0;
		//actualPoints = 0;
	}
	
	// Update is called once per frame
	void Update () {
		tilesText.text = "TILES: " + tiles;
	}
}
