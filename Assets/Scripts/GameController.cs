﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

	public int numOfTilesWidth;
	public int numOfTilesHeight;
	public int numOfCorrectTiles;
	public GameObject tilesCreator;
	public int numOfRounds;
	public int actualRound;
	public GameObject playAgain;
	private int maxTiles;
	private GameObject creator;
	private int widthOrHeight;
	private int numOfMistakes;


	public GameObject pointsPlus;
	// Use this for initialization
	void Start () {
		numOfRounds += 1;
		creator = Instantiate (tilesCreator);
		widthOrHeight = 1;
		numOfMistakes = 0;
	}
	// Update is called once per frame
	void Update () {
		GameObject g = GameObject.FindGameObjectWithTag("Tiles");
		TilesController otherScript = g.GetComponent<TilesController>();
		if (otherScript.isThisEnd==true) {
			if(otherScript.correctClicked==otherScript.numOfTries){
				if(maxTiles<numOfCorrectTiles){
					maxTiles=numOfCorrectTiles;
				}
				numOfMistakes=0;
				if(widthOrHeight==1){
					numOfTilesWidth+=1;
					widthOrHeight=0;
				}
				else{
					numOfTilesHeight+=1;
					widthOrHeight=1;
				}
				numOfCorrectTiles+=1;
			}
			else{
				numOfMistakes+=1;
				if(numOfMistakes==2){
					if(widthOrHeight==1){
						numOfTilesHeight-=1;
						widthOrHeight=0;
					}
					else{
						numOfTilesWidth-=1;
						widthOrHeight=1;
					}
					numOfMistakes=0;
					numOfCorrectTiles-=1;
					if(numOfCorrectTiles<2){
						numOfCorrectTiles=2;
					}
					if(numOfTilesHeight<2){
						numOfTilesHeight=2;
					}
					if(numOfTilesWidth<2){
						numOfTilesWidth=2;
					}
				}
			}
			Destroy(creator);
			creator=Instantiate(tilesCreator);
		}
		if (actualRound == numOfRounds) {
			Debug.Log("END");
			Instantiate(playAgain);
			GameObject i = GameObject.FindGameObjectWithTag("Ui");
			GameObject j = GameObject.FindGameObjectWithTag("Tiles");
			Destroy(i);
			Destroy(j);
			GameObject p = GameObject.Find("EndPoints");
			PointsController pscript = p.GetComponent<PointsController> ();
			GameObject h = GameObject.FindGameObjectWithTag("Points");
			PointsController otherScripth = h.GetComponent<PointsController>();
			pscript.points=otherScripth.points;
			pscript.pointsText.enabled = true;
			GameObject t = GameObject.Find("BestTilesResult");
			TilesUiController tscript = t.GetComponent<TilesUiController> ();
			tscript.tiles=maxTiles;
			tscript.tilesText.enabled = true;
			Destroy(this.gameObject);
		}
	}
}