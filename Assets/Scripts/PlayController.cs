﻿using UnityEngine;
using System.Collections;

public class PlayController : MonoBehaviour {
	public GameObject ui;
	public GameObject game;

	void OnMouseDown(){
		GameObject p = GameObject.Find("EndPoints");
		PointsController pscript = p.GetComponent<PointsController> ();
		pscript.pointsText.enabled = false;
		
		GameObject t = GameObject.Find("BestTilesResult");
		TilesUiController tscript = t.GetComponent<TilesUiController> ();
		tscript.tilesText.enabled = false;

		Instantiate (ui);
		Instantiate (game);
		Destroy (this.gameObject);
	}
}
