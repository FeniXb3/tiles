﻿using UnityEngine;
using System.Collections;

public class PointsController : MonoBehaviour {
	
	public GUIText pointsText;
	public int points;
	private int actualPoints;

	void Start () {
		pointsText.fontSize = Screen.height / 20;
		//pointsText.enabled = false;
		points = 0;
		actualPoints = 0;
	}
	void Update () {
		//if(actualPoints<po
		if (actualPoints < points) {
			actualPoints+=1;
		}
		pointsText.text = "SCORE: " + actualPoints;
	}
}
