﻿using UnityEngine;
using System.Collections;

public class InstantiateTileController : MonoBehaviour {
	public bool isThisRightTile;
	public Sprite rightTile;
	public Sprite wrongTile;
	public Sprite backTile;
	public int tileNumber;
	public int allTiles;
	public float waitTime;
	public float showingTime;
	public bool isClickable;
	public bool wasShowed;
	private SpriteRenderer sr;
	private float startTime;
	private bool showed;
	private float timeToShow;
	private Rigidbody2D rigid;
	//private BoxCollider2D col;

	void Start () {
		//col = GetComponent<BoxCollider2D> ();
		//col.enabled = false;
		sr = GetComponent<SpriteRenderer>();
		rigid=GetComponent<Rigidbody2D>();
		//rigid.isKinematic = false;
		startTime = Time.time;
		showed = false;
		wasShowed = false;
		timeToShow = 0;
//		Debug.Log (transform.position.x);
//		Debug.Log (transform.position.y);
		//col.enabled = true;
	}
	void Update () {
		if (wasShowed == false) {
			if(showed==false){
				float temp = startTime + waitTime;
				if (temp < Time.time) {
					if (isThisRightTile == true) {
						sr.sprite = rightTile;
					} else {
						sr.sprite = wrongTile;
					}
					showed=true;
				}
			}
			if(showed==true){
				float temp = startTime + waitTime+showingTime;
				if (temp < Time.time) {
					isClickable=true;
					wasShowed=true;
					sr.sprite=backTile;
				}
			}
		}
		GameObject g = GameObject.FindGameObjectWithTag("Tiles");
		TilesController otherScript = g.GetComponent<TilesController>();
		if (otherScript.showSecondTime == true) {
			if(otherScript.gravity==true){
				float tempgrav=Random.Range(0.5f,5f);
				rigid.gravityScale=tempgrav;
			}
			if(timeToShow==0){
				timeToShow=Time.time+1f;
			}
			if(timeToShow<Time.time){
				if (isThisRightTile == true) {
					sr.sprite = rightTile;
					sr.transform.rotation = Quaternion.Euler (0.0f, 0.0f, 0.0f);
				}
			}
		}
	}
	void OnMouseDown(){
		GameObject g = GameObject.FindGameObjectWithTag("Tiles");
		TilesController otherScript = g.GetComponent<TilesController>();
		if(otherScript.clickable==true){
			if(isClickable==true){
				otherScript.numOfTries+=1;
				if (isThisRightTile == true) {
					GameObject h = GameObject.FindGameObjectWithTag("Points");
					PointsController otherScripth = h.GetComponent<PointsController>();
					otherScripth.points+=10;
					otherScript.correctClicked+=1;
					sr.sprite = rightTile;
				} else {
					sr.sprite = wrongTile;
				}
				sr.transform.rotation = Quaternion.Euler (0.0f, 0.0f, 0.0f);
				isClickable=false;
			}
		}
	}
}