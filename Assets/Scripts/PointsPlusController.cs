﻿using UnityEngine;
using System.Collections;

public class PointsPlusController : MonoBehaviour {
	public GUIText pointsText;
	public int points;
	private float alpha;
	private float speed;
	// Use this for initialization
	void Start () {
		pointsText.fontSize = Screen.height / 20;
		//pointsText.enabled = false;
		alpha = 1.0f;
		speed = 0.0f;
		points = 20;
		pointsText.text = "+ " + points;
	}
	
	// Update is called once per frame
	void Update () {
		//pointsText.text = "+ " + points;
		speed += 0.0005f;
		this.gameObject.transform.position += new Vector3 (speed, 0.0f, 0.0f);
		alpha -= 0.025f;
		Color32 color = new Color (pointsText.material.color.r, pointsText.material.color.g, pointsText.material.color.b, alpha);
		pointsText.material.color = color;
		if (alpha <= 0) {
			Destroy(this.gameObject);
		}
	}
}
